# Installation

1. Download this code.
2. Create a virtual environment: (e.g., depending on your system, see below) `python3 -m venv .venv` and activate it (`source .venv/bin/activate`).
3. Install the dependencies: `pip install -r requirements.txt`
4. Start the notebook process: `jupyter notebook .`

## Virtualenvs

For instructions how to install virtual environments on your system, see https://docs.python.org/3/library/venv.html#creating-virtual-environments.
Install a virtualenv that includes pip.

# Executing the code

Use the jupyter notebook to run the code snippets, update them to your needs and create the files.

# Notes

At least Python 3.6 is required for this code.

The `requests_cache` library creates an sqlite database file inside this directory to save the html requests so that they only have to be downloaded once. Delete this file if you want to delete or recreate the cache. A zipped requests_cache.sqlite is part of this repository for convenience and it must be unzipped before running the code from the jupyter notebook. If it is not used, the first data retrieval processes will take some time.

# Data

Instead of running the code on your own, you can also have a look at the data files (.xlsx, .gexf, .gephi) inside this directory.
